/**
 * 
 * @param link
 * @return the jQuery selector to use in finding the checkboxes we are attempting to manipulate
 */
function find_matching_checkboxes(link) {
	var module = $(link).attr('rel');
	var ctd = $(link).parent('td');
	// get the index of the current column
	var col_index = $('tr.'+module+' td').index(ctd);
	col_index++;
	return 'tr.'+module+' td:nth-child('+col_index+') input:checkbox';
}

$(document).ready(function(){
  
  // get number of columns we will need to manipulate & add 
  // select / unselect options for each module header row
  var roles = $('#permissions > thead > tr > th.checkbox');
  var role_count = roles.size();
  // take off the stupid colspan from the module header row, because we 
  // will be matching up to the other rows perfectly now
  $('td.module').removeAttr('colspan')
  // needed to add in a defining class or id to the parent TR in 
  // order to have a reference to grab a proper index for the links
  // being clicked.
  $('td.module').each(function(){
    var module_id = $(this).attr('id');
    $(this)
    	.parent('tr')
    	.addClass(module_id+' module_parent')
    	.attr('rel', module_id);
  });
  // give the primary TD a good class name as well
  $('td.module').addClass('module_name');
  // cycle how many roles we have and insert that many columns worth of 
  // select / deselect options
  $(roles).each(function(){
  $('td.module')
   		.after('<td class="pselect"><a href="#" class="check">check all</a> / <a href="#" class="uncheck">uncheck all</a></td>');
  });
  // give the new boxes the module class to preserve styling of the row
  $('td.pselect').addClass('module');
  // let's do something to assign a variable class to the checkboxes
  // so that they can be referenced easily as well
  // this will get the value IF it is the first-child of the "group"
  var lastvalue;
  var parent_trrel;
  $('td.checkbox').each(function(){
  lastvalue = parent_trrel;
  parent_trrel = $(this)
                          .parent('tr')
                          .prev('.module_parent')
                          .attr('rel');
    // if there wasn't a direct previous TR with an appropriate
    // class to grab, just use the last proper one we found. pimpin'
    if(!parent_trrel) {
      parent_trrel = lastvalue;
    }
    $(this).addClass(parent_trrel).parent('tr').addClass(parent_trrel);
  });
  // let's actually loop the href's we've created
  // and give them an extra class too
  $('a.check, a.uncheck').each(function(){
    // getting the class name we assigned to the row above the clicked link
    var parent_module = $(this)
     				.parent('td')
                    .parent('tr.module_parent')
                    .attr('rel');
      $(this).attr('rel', parent_module);
  });
  // assign click functions to all the check/uncheck links on a per
  // module basis  
  $('a.check').click(function(){
	var check_it = find_matching_checkboxes(this);
	$(check_it).attr('checked', true);
	return false;	
  });
  $('a.uncheck').click(function(){
	var uncheck_it = find_matching_checkboxes(this);
	$(uncheck_it).attr('checked', false);
	return false;	
  });  
});
